﻿using System.Collections.Generic;
using System.Data.Services;
using System.Data.Services.Common;
using System.Linq;

namespace WarmCrocWebApplication
{
    public class ComplexPOCO : DataService<ComplexAnimals>
    {
        public static void InitializeService(DataServiceConfiguration config)
        {
            config.SetEntitySetAccessRule("*", EntitySetRights.AllRead);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V3;
        }
    }

    public class ComplexAnimals
    {
        readonly List<ComplexAnimal> _complexAnimalList = new List<ComplexAnimal>();

        public ComplexAnimals()
        {
            _complexAnimalList.Add(new Crocodile        {ID = 0, Name = "Crocodile", NumberOfTeeth = (int) 10E6});
            _complexAnimalList.Add(new ComplexAnimal    { ID = 1, Name = "Racoon" });
            _complexAnimalList.Add(new Chicken          { ID = 2, Name = "Chicken", Isblind = true, CanFindSeed = true});
        }

        public IQueryable<ComplexAnimal> MyComplexAnimals
        {
            get { return _complexAnimalList.AsQueryable(); }
        }
    }

    [DataServiceKey("ID")]
    public class ComplexAnimal
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class Crocodile : ComplexAnimal
    {
        public int NumberOfTeeth { get; set; }
    }

    public class Chicken : ComplexAnimal
    {
        public bool CanFindSeed { get; set; }
        public bool Isblind { get; set; }
    }
}
