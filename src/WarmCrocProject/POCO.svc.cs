﻿using System.Collections.Generic;
using System.Data.Services;
using System.Data.Services.Common;
using System.Linq;

namespace WarmCrocWebApplication
{
    public class POCO : DataService<Animals>
    {
        public static void InitializeService(DataServiceConfiguration config)
        {
            config.SetEntitySetAccessRule("*", EntitySetRights.All);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V3;
        }
    }

    public class Animals
    {
        readonly List<Animal> _animalList = new List<Animal> ();

        public Animals()
        {
            _animalList.Add(new Animal {ID = 0, Length = 5.3d, Name = "Crocodile"});
            _animalList.Add(new Animal {ID = 1, Length = 1, Name = "Racoon"});
        }

        public IQueryable<Animal> MyAnimals
        {
            get { return _animalList.AsQueryable(); }
        }
    }

    [DataServiceKey("ID")]
    public class Animal
    {
        public int ID { get; set; }
        public double Length { get; set; }
        public string Name { get; set; }
    }


}
