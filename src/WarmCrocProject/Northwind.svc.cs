﻿using System;
using System.Collections.Generic;
using System.Data.Services;
using System.Data.Services.Common;
using System.Linq;
using System.ServiceModel.Web;
using System.Web;
using WarmCrocWebApplication;

namespace WarmCrocConfApp
{
    public class Northwind : DataService<WarmCrocEntities>
    {
        // This method is called only once to initialize service-wide policies.
        public static void InitializeService(DataServiceConfiguration config)
        {
            config.UseVerboseErrors = true;
            config.SetEntitySetAccessRule("*", EntitySetRights.All);
            config.SetServiceOperationAccessRule("GetSupplierByProductID", ServiceOperationRights.All);
            config.SetServiceOperationAccessRule("GetOrdersByCity", ServiceOperationRights.All);
            config.SetServiceOperationAccessRule("GetPostOrdersByCity", ServiceOperationRights.All);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V3;
        }

        [WebGet]
        public IQueryable<Order> GetOrdersByCity(string city)
        {
            var context = new WarmCrocEntities();

            return context.Orders.Where(o => o.ShipCity == city);
        }

        [WebInvoke(Method = "POST")]
        public IQueryable<Order> GetPostOrdersByCity(string city)
        {
            var context = new WarmCrocEntities();

            return context.Orders.Where(o => o.ShipCity == city);
        }


        [WebGet]
        public IQueryable<Supplier> GetSupplierByProductID(int productID)
        {
            var context = new WarmCrocEntities();

            var suppliers = context.Products
                .Where(p => p.ProductID == productID)
                .Select(i => i.Supplier);
            return suppliers.AsQueryable();
        }

    }
}
